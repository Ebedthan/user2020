+++
title = "Instrucciones de Registro"
description = "Registering through the CloudCME portal"
keywords = ["tickets","registration"]
+++ 
## Resumen
Gracias por su interés en asistir a useR! 2020. ¡Nos alegra mucho tenerle con nosotros! Para registrarse, usted tendrá que [crear una cuenta](https://slu.cloud-cme.com/default.aspx?P=5&EID=11480) con nuestro vendedor de inscripción, Saint Louis University School of Medicine. Cuando ya haya creado una cuenta (pasos 1-5), puede dirigirse nuevamente a nuestro [portal de registro](https://slu.cloud-cme.com/default.aspx?P=5&EID=11480) y proceder con el proceso de registro. Cuando lo termine, ¡no olvide reservar su habitación en [Marriott St. Louis Grand](https://book.passkey.com/event/49973088/owner/84420/landing) a una tarifa reducida para el congreso!

## Detalles
1. Dirigirse a <a href="https://slu.cloud-cme.com/default.aspx?P=5&EID=11480" target="_blank">la página principal de registro</a>.
2. Hacer clic en “Sign In”.  
![picture of sign in](/registration/fig1_edit.png)  
3. Hacer clic en “Don’t Have An Account?”  
![picture of don't have account](/registration/fig2_edit.png)  
4. Rellenar el formulario en la siguiente pantalla, eligiendo: 
   1. “Other” para “Select Degree”
   2. “Non-Physician” para “Select Profession”
   3. “NON CME ACTIVITY” para “Select Primary Credit Eligibility”  
![picture of correct choices](/registration/fig3.png)  
5. Hacer clic en “Create Account”. 
6. Dirigirse nuevamente a <a href="https://slu.cloud-cme.com/default.aspx?P=5&EID=11480" target="_blank">la página principal</a> y hacer clic en “Register”, iniciando la sesión con su información de cuenta si se lo pide.
7. Rellenar todos los campos requeridos y seleccionar “Industry”, “Academic” o “Student” para registrarse. Los inscritos académicos y estudiantiles deberían estar dispuestos a mostrar prueba de su afiliación universitaria en la mesa de inscripción en el congreso. Registrarse como un “diversity scholar” (becado de diversidad) o “organizational team member” (miembro del comité organizativo) requiere aprobación previa. 
8. Después de seleccionar un producto principal de registro, por favor seleccione tantos boletos para la “Gala Dinner” (cena de gala) como quiera. Los boletos para la cena de gala incluyen acceso al [City Museum](https://www.citymuseum.org) después de la cena, lo cual estará abierto exclusivamente para los asistentes de useR! 2020 de 8pm a 11pm. 
9. Por favor seleccione hasta dos tutoriales (uno por la mañana y uno por la tarde) y cualquier extra gratuito que quiera para los eventos sociales. 
10.  Por favor complete nuestra encuesta, la cual incluye aceptar el [código de conducta](https://user2020.r-project.org/codeofconduct/). Una vista preliminar de la [camiseta del congreso](/registration/tshirt/) está disponible. 
11.  Por último, por favor complete el pago. 
12.  Cuando termine, diríjase a <a href="https://book.passkey.com/event/49973088/owner/84420/landing" target = "_blank">Marriott St. Louis Grand</a> para reservar su habitación en el hotel del congreso a una tarifa reducida.
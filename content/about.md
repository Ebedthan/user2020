+++
title = "About"
description = "Information about the organisers"
keywords = ["about","organisers","staff","R consortium"]
+++

### <i class="fas fa-users"></i> What is the useR! conference?
Actively supported by the [R Foundation](https://www.r-project.org/conferences/), the useR! is an annual, international conference of leading statisticians and data scientists from
around the world. In recent years, it has been held in Toulouse, France; Brisbane, Australia;
Brussels, Belgium; and Stanford, California. The conference is the main annual meeting of
the R community, and attendance in 2020 is anticipated to be over 1,000 people. Attendees
will include R developers and users who are data scientists, business intelligence specialists,
analysts, statisticians from academia and industry, and students.

<br>
### <i class="fab fa-r-project"></i> What is R?
R is a programming language that offers a free, open source platform for data analytics,
statistics, visualization, geospatial analyses, mapping, scientific communication, and
dashboard creation. It is one of the most popular tools in data science, with over 50% of
data analysis professionals using R in their workflow.

<br>
### <i class="fas fa-map-marker-alt"></i> When and where will the conference be held?
The conference will be free and online. Regular and keynote talks will be on YouTube. How to register for tutorials will be communicated shortly. 

<br>
###  <i class="fas fa-envelope-open-text"></i> How can I stay up to date?
Please check back often, as our website will be updated regularly. You can also [follow us on Twitter](http://twitter.com/useR2020stl) and sign-up for occasional [email updates via Mailchimp](https://mailchi.mp/60a9505c9965/user2020).

<br>
### <i class="fas fa-hand-holding-usd"></i> Are there sponsorship opportunities?
Sponsorship of useR! 2020 is an opportunity to support your data science team
development, underwrite the continued development of a tool in your analytics pipeline,
and continue to connect your company to the R user community. It is also an opportunity to
connect with future data science hires who may be interested in joining your team. If you are
interested in details about sponsorship, please [contact](/contact/) the organizing team.
